﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseClickScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 mouse=Input.mousePosition;
		if(Input.GetMouseButtonDown(0)){
			Debug.Log("left mouse button down: ("+mouse.x+", "+mouse.y+")");
		}
		if(Input.GetMouseButtonDown(1)){
			Debug.Log("right mouse button down: ("+mouse.x+", "+mouse.y+")");
		}
		if(Input.GetMouseButtonDown(2)){
			Debug.Log("middle mouse button down: ("+mouse.x+", "+mouse.y+")");
		}

	}
}
