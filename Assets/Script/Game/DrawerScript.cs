﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawerScript : MonoBehaviour {

	// Use this for initialization
	bool open;
	void Start () {
		open=false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnMouseDown(){
		if(open==false){
			transform.Translate(new Vector3(0.8f,0,0));
			open=true;
		}
		else{
			transform.Translate(new Vector3(-0.8f,0,0));
			open=false;
		}
	}
}
