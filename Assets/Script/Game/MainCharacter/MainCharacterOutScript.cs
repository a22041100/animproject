﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCharacterOutScript : MonoBehaviour {

	public float turnRate=2;
    public float upRotLit=750;
    public float downRotLit=15;
    float rotated=0;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 rotate=new Vector3(0,0,0);
		Vector3 mousePos=Input.mousePosition;
		
        if(mousePos.y<downRotLit && rotated<30){
            rotate.x=turnRate;
        }
        else if(mousePos.y>upRotLit && rotated>-30){
            rotate.x=-turnRate;
        }
        else{
            rotate.x=0;
		}
		transform.Rotate(rotate);
        rotated+=rotate.x;
	}
}
