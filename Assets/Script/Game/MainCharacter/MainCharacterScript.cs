﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCharacterScript : MonoBehaviour {

    public float walkSpeed=7f;
    public float runSpeed=22f;
    public float turnRate=2;
    public float downHeight=2;
    public float leftRotLit=30;
    public float rightRotLit=1000;

    Rigidbody rigidbody;
	CapsuleCollider collider;
    // Use this for initialization
    void Start () {
        rigidbody = GetComponent<Rigidbody>();
		collider = GetComponent<CapsuleCollider>();
    }
    
    // Update is called once per frame
    void Update () {
        float horizontalSpeed=0;
        float verticalSpeed=0;
        float height=0;
        Vector3 rotate=new Vector3(0,0,0);
        Vector3 translate=new Vector3(0,0,0);
		Vector3 colcenter=collider.center;

        bool jump=Input.GetKey(KeyCode.Q);
        bool run=Input.GetKey(KeyCode.LeftShift);
        bool down=Input.GetKeyDown(KeyCode.E);                                      
		bool downN=Input.GetKeyUp(KeyCode.E);

        Vector3 mousePos=Input.mousePosition;

        verticalSpeed = Input.GetAxisRaw("Vertical");
        horizontalSpeed=Input.GetAxisRaw("Horizontal");

        if(run==true){
            verticalSpeed*=runSpeed;
            horizontalSpeed*=runSpeed;
        }
        else{
            verticalSpeed*=walkSpeed;
            horizontalSpeed*=walkSpeed;
        }

        translate=transform.forward*verticalSpeed + transform.right*horizontalSpeed;
        
		if(mousePos.x>rightRotLit){
				rotate.y=turnRate;
			}
		else if(mousePos.x<leftRotLit){
				rotate.y=-turnRate;
			}
		else{
				rotate.y=0;
			}

		if(down==true){
			//colcenter.y-=0.5f;
			//collider.center=colcenter;
			collider.height-=1.5f;
			transform.Translate(new Vector3(0,-0.8f,0));
		}
		else if(downN==true){
			//colcenter.y+=0.5f;
			//collider.center=colcenter;
			collider.height+=1.5f;
			transform.Translate(new Vector3(0,0.8f,0));
		}
		collider.center=colcenter;
		
		if(jump==true && transform.position.y<2.55f){
			rigidbody.AddForce(new Vector3(0,5,0), ForceMode.Impulse);
		}

		MainGameScript.PlayerPos=rigidbody.position;
		MainGameScript.PlayerDir=transform.forward;
		if(Input.GetMouseButtonDown(0)){
        	MainGameScript.MouseDir= Camera.main.ScreenPointToRay(Input.mousePosition).direction;
        }
		
        transform.Rotate(rotate);
		//if(MainGameScript.PlayerMove==true){
        	rigidbody.MovePosition(rigidbody.position+translate*Time.fixedDeltaTime);
		//}
    }
}

