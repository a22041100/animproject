﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicForceScript : MonoBehaviour {

	Rigidbody rigidbody;
	Vector3 direction=new Vector3(0,0,0);
	Vector3 origin;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		origin=transform.position;

	}


	void OnMouseOver(){
		if(Input.GetMouseButtonDown(0)){
			direction=(MainGameScript.PlayerDir+MainGameScript.MouseDir)*5;
			rigidbody.AddForce(direction, ForceMode.Impulse);
		}
		else if(Input.GetMouseButtonDown(1)){
			direction=-(MainGameScript.PlayerDir+MainGameScript.MouseDir)*3;
			rigidbody.AddForce(direction, ForceMode.Impulse);
		}
		else if(Input.GetMouseButton(2)){
			transform.position=origin;
		}
	}
}
