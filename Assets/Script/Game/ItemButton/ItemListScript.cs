﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemListScript : MonoBehaviour {
	public List<Image> imageList=new List<Image>();
	public List<Sprite> itemList = new List<Sprite>();
	List<bool> itemBoolList=new List<bool>();
	public Sprite background;
	public int numberOfItems;
	// Use this for initialization
	void Start () {
		for(int i=0;i<numberOfItems;i++){		
			imageList[i].sprite=background;
		}
		for(int i=0;i<MainGameScript.CollectableObj.Count;i++){
			itemBoolList.Add(MainGameScript.CollectableObj[i].GetCollected());
		}
	}
	// Update is called once per frame
	void Update () {
		for(int i=0;i<MainGameScript.CollectableObj.Count;i++){
			bool temp=MainGameScript.CollectableObj[i].GetCollected();
			if(itemBoolList[i]!=temp){
				itemBoolList[i]=temp;
				if(temp==true){
					SetToList(i);
				}
				else{
					SetToBg(i);
				}

			}
		}

	}

	void SetToList(int i){
		for(int j=0;j<5;j++){
			if(imageList[j].sprite==background){
				imageList[j].sprite=itemList[i];
				break;
			}
		}
	}
	void SetToBg(int i){
		for(int j=0;j<5;j++){
			if(imageList[j].sprite==itemList[i]){
				imageList[j].sprite=background;
				imageList[i].color=Color.white;
				break;
			}
		}
	}
}
