﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour {
	bool click;
	public Image image;
	int number=-1;

	// Use this for initialization
	void Start () {
		click=false;
	}
	
	// Update is called once per frame
	void Update () {
		int tempnum=-1;
		for(int i=0;i<MainGameScript.CollectableObj.Count;i++){
			if(MainGameScript.CollectableObj[i].GetName().Contains(image.sprite.name)){
				tempnum=i;
				break;
			}
		}
		number=tempnum;
	}
	public void ChangeColor(){

		if(click==false && MainGameScript.CollectableObj[number].GetCollected()==true){
			click=true;
			image.color=Color.grey;
			MainGameScript.CollectableObj[number].SetCarry(true);
		}
		else{
			click=false;
			image.color=Color.white;
			MainGameScript.CollectableObj[number].SetCarry(false);
		}
	}
}
