﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonTestScript : MonoBehaviour {

	public Image image;
	public Sprite imageOrigin;
	public Sprite imageClick;
	public void changeImage(){
		if(image.sprite==imageOrigin){
			image.sprite=imageClick;
		}
		else{
			image.sprite=imageOrigin;
		}
	}
}
