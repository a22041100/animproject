﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameScript : MonoBehaviour {
	public class Collectable{
		int index;
		string name;
		bool collected;
		bool carry;
		bool used;
		public Collectable(int i, string n){
			this.index=i;
			this.name=n;
			this.collected=false;
			this.carry=false;
			this.used=false;
		}
		public int GetIndex(){
			return index;
		}
		public string GetName(){
			return name;
		}
		public bool GetCollected(){
			return collected;
		}
		public bool GetCarry(){
			return carry;
		}
		public bool GetUsed(){
			return used;
		}

		public void SetCollected(bool value){
			collected=value;
		}
		public void SetCarry(bool value){
			carry=value;
		}
		public void SetUsed(bool value){
			used=value;
		}
	}

	public static bool PlayerMove;
	public static Vector3 PlayerPos;
	public static Vector3 PlayerDir;
	public static Vector3 MouseDir;

	public static bool TakeBook;
	public static int BookPage;
	public static string BookReading;

	public static List<Collectable> CollectableObj=new List<Collectable>();

	public static bool DinnerTime;
	public static bool GameFinish;
	
	// Use this for initialization
	void Start () {
		PlayerMove=true;
		TakeBook=false;
		BookPage=0;
		for(int i=0;i<CollectableObj.Count;i++){
			CollectableObj[i].SetCollected(false);
			CollectableObj[i].SetCarry(false);
			CollectableObj[i].SetUsed(false);
		}
		DinnerTime=false;
		GameFinish=false;
	}

	void Awake(){
		Reset();

	}
	
	// Update is called once per frame
	void Update () {
	}

	public void Reset(){
		CollectableObj.Add(new Collectable(0, "HomeDoorLockKey"));
		CollectableObj.Add(new Collectable(1, "KitchenDoorLockKey"));
		CollectableObj.Add(new Collectable(2, "Room5DoorLockKey"));
		CollectableObj.Add(new Collectable(3, "Room9DoorLockKey"));
		CollectableObj.Add(new Collectable(4, "DinHallDoorLockKey"));
	}
}


