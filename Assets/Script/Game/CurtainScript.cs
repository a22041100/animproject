﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurtainScript : MonoBehaviour {
	bool opened;
	// Use this for initialization
	void Start () {
		opened=false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnMouseDown(){
		if(opened==false){
			transform.localScale -= new Vector3(0, 0, 0.4f);
			opened=true;
		}
		else{
			transform.localScale += new Vector3(0, 0, 0.4f);
			opened=false;
		}
	}
}
