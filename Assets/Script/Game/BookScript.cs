﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookScript : MonoBehaviour {

	Rigidbody rigidbody;
	Vector3 OriginalPos;
	public static bool taken;

	float Ytrans;
	int MaxPage;
	// Use this for initialization
	void Start () {
		rigidbody=GetComponent<Rigidbody>();
		OriginalPos=rigidbody.position;
		taken=false;
		if(name.Contains("BookR")){
			Ytrans=-2.8f;
		}
		else if(name.Contains("BookO")){
			Ytrans=0.7f;
		}
		else if(name.Contains("BookG")){
			Ytrans=-0.7f;
		}
		else if(name.Contains("BookB")){
			Ytrans=-1.4f;
		}
		else if(name.Contains("BookP")){
			Ytrans=-2.1f;
		}
		else{
			Ytrans=0;
		}

		if(name.Contains("Box")){
			MaxPage=1;
		}
		else{
			MaxPage=7;
		}

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnMouseDown(){
		if(MainGameScript.TakeBook==false){
			//rigidbody.MovePosition(MainGameScript.PlayerPos+MainGameScript.PlayerDir+new Vector3(0,-0.8f,0));
			rigidbody.constraints = RigidbodyConstraints.None;
			transform.Translate(new Vector3(-1,Ytrans,0));
			transform.Rotate(new Vector3(0,135,0));
			transform.Rotate(new Vector3(15,0,0));
			transform.Rotate(new Vector3(0,0,15));
			rigidbody.constraints = RigidbodyConstraints.FreezeAll;
			MainGameScript.TakeBook=true;
			MainGameScript.BookReading=name;
			//this.gameObject.transform.GetChild(0).transform.Rotate(new Vector3(0,85,0));
			//MainGameScript.PlayerMove=false;
		}
		else if(MainGameScript.BookReading==name){
			if(MainGameScript.BookPage<MaxPage){
				TurnPage(MainGameScript.BookPage);
				MainGameScript.BookPage++;
			}
			else if(MainGameScript.BookPage==MaxPage){
				for(int i=0;i<MaxPage;i++){
					TurnPageBack(i);
				}
				MainGameScript.BookPage++;
			}
			else{
				rigidbody.constraints = RigidbodyConstraints.None;
				//rigidbody.MovePosition(OriginalPos);
				transform.Rotate(new Vector3(0,0,-15));
				transform.Rotate(new Vector3(-15,0,0));
				transform.Rotate(new Vector3(0,-135,0));
				transform.Translate(new Vector3(1,-Ytrans,0));

				rigidbody.constraints = RigidbodyConstraints.FreezeAll;
				MainGameScript.TakeBook=false;
				MainGameScript.BookPage=0;
			}
		}

	}

	void TurnPage(int child){
			transform.GetChild(child).transform.Rotate(new Vector3(0,90-child*2,0));
	}

	void TurnPageBack(int child){
		transform.GetChild(child).transform.Rotate(new Vector3(0,-90+child*2,0));
	}
}
