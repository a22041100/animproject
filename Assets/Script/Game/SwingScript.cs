﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwingScript : MonoBehaviour {
	float rotate;
	bool dir;

	// Use this for initialization
	void Start () {
		rotate=0;
		dir=true;
	}
	
	// Update is called once per frame
	void Update () {
		if(MainGameScript.DinnerTime==false){
			if(dir==true){
				if(rotate<15){
					transform.Rotate(new Vector3(0,0,0.5f));
					rotate+=0.5f;
				}
				else{
					dir=false;
				}
			}
			else{
				if(rotate>-15){
					transform.Rotate(new Vector3(0,0,-0.5f));
					rotate-=0.5f;
				}
				else{
					dir=true;
				}
			}
		}
	}
}
