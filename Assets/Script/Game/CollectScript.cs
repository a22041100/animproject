﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectScript : MonoBehaviour {
	int number=-1;

	// Use this for initialization
	void Start () {
		for(int i=0;i<MainGameScript.CollectableObj.Count;i++){
			if(MainGameScript.CollectableObj[i].GetName().Contains(this.name)){
				number=i;
				Debug.Log(this.name+" is "+number+" now");
				break;
			}
		}

		if(number==-1){
			Debug.Log("the number of "+this.name+" is -1, ERRORRR");
		}
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnMouseDown(){
		string name=this.name;
		MainGameScript.CollectableObj[number].SetCollected(true);
		gameObject.SetActive(false);
		
	}
}
