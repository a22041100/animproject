﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockScript : MonoBehaviour {
	int number=-1;
	// Use this for initialization
	void Start () {
		for(int i=0;i<MainGameScript.CollectableObj.Count;i++){
			if(MainGameScript.CollectableObj[i].GetName().Contains(this.name)){
				number=i;
				Debug.Log(this.name+" is "+number+" now");
				break;
			}
		}
		if(number==-1){
			Debug.Log("the number of "+this.name+" is -1, ERRORRR");
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnMouseDown(){
		Debug.Log("You click the "+this.name);
		if(MainGameScript.CollectableObj[number].GetCarry()==true){
			Debug.Log("There is a key");
			MainGameScript.CollectableObj[number].SetCollected(false);
			MainGameScript.CollectableObj[number].SetCarry(false);
			MainGameScript.CollectableObj[number].SetUsed(true);
		}
		else{
			Debug.Log("There is no key");
		}
	}
}
