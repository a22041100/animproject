﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinMessageScript : MonoBehaviour {
	public GameObject text1;
	public GameObject text2;
	public GameObject text3;
	public GameObject text4;
	public GameObject button;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(MainGameScript.GameFinish==true){
			text1.SetActive(true);
			text2.SetActive(true);
			text3.SetActive(true);
			text4.SetActive(true);
			button.SetActive(true);
		}
	}
}
