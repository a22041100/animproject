﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creature9Script : MonoBehaviour {
	public static int state;
	Rigidbody rigidbody;
	Vector3 OriginalPosition;
	RaycastHit hit;
	int rotatedSearch=0;
	int rotated=0;
	bool searching;

	// Use this for initialization
	void Start () {
		state=0;
		rigidbody=GetComponent<Rigidbody>();
		OriginalPosition=rigidbody.position;
		searching=false;
		
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 target = new Vector3(0,0,0);

		if(searching==true){
			if(forwCast(hit)){
				transform.Translate(new Vector3(0,0,0.5f), Space.Self);
				Debug.Log("go front");
			}
			else{
				switch(sideCast(hit)){
					case 1:
						transform.Rotate(new Vector3(0,8,0));
						rotatedSearch-=8;
				Debug.Log("go leftleft"+rotatedSearch);
						break;
					case 2:
						transform.Rotate(new Vector3(0,3,0));
						rotatedSearch-=3; 
				Debug.Log("go leftmid"+rotatedSearch);
						break;
					case 3:
						transform.Rotate(new Vector3(0,-3,0));
						rotatedSearch+=3;
				Debug.Log("go rightmid"+rotatedSearch);
						break;
					case 4: 
						transform.Rotate(new Vector3(0,-8,0));
						rotatedSearch+=8;
				Debug.Log("go rightright"+rotatedSearch);
						break;
					default:
						if(rotatedSearch<360){
							transform.Rotate(new Vector3(0,-15,0));
							rotatedSearch+=15;
				Debug.Log("go whatever"+rotatedSearch);
						}
						else{
							searching=false;
							rotatedSearch=0;
				Debug.Log("back to work"+rotatedSearch);
						}
						break;
				}
			}

		}
		else{

		if(Physics.Raycast(rigidbody.position+new Vector3(0,2,1),transform.forward,out hit) && hit.collider.gameObject.name.Contains("Camera")){
				searching=true;
		}
		else if(Physics.Raycast(rigidbody.position+new Vector3(0,2,1),transform.forward+new Vector3(0,-0.2f,0),out hit) && hit.collider.gameObject.name.Contains("Camera")){
				searching=true;
		}

		switch(state){
			case 0:
				if(MainGameScript.DinnerTime==true){
					state++;
				} 
				break;
			case 1:
			target=new Vector3(30.6f, 0.49f, 18.8f);
				transform.position=Vector3.MoveTowards(rigidbody.position, target, 0.3f);
				if(Vector3.Distance(rigidbody.position, target)<1){
					state++;
				}
				break;
			case 2:
			if(rotated<75){
					transform.Rotate(new Vector3(0,-5,0));
					rotated+=5;
				}
				else{
					state++;
					rotated=0;
				}
				break;
			break;
			case 3:
			target=new Vector3(40f, 0.49f, 18.34f);
				transform.position=Vector3.MoveTowards(rigidbody.position, target, 0.3f);
				if(Vector3.Distance(rigidbody.position, target)<1){
					state++;
				}
				break;
			case 4:
			if(rotated<90){
					transform.Rotate(new Vector3(0,5,0));
					rotated+=5;
				}
				else{
					state++;
					rotated=0;
				}
				break;
			case 5:
			target=new Vector3(40f, 0.49f, -40f);
				transform.position=Vector3.MoveTowards(rigidbody.position, target, 0.3f);
				if(Vector3.Distance(rigidbody.position, target)<1){
					state++;
				}
				break;
			case 6:
			if(rotated<90){
					transform.Rotate(new Vector3(0,5,0));
					rotated+=5;
				}
				else{
					state++;
					rotated=0;
				}
				break;
			case 7:
			target=new Vector3(-39.3f, 0.49f, -40f);
				transform.position=Vector3.MoveTowards(rigidbody.position, target, 0.3f);
				if(Vector3.Distance(rigidbody.position, target)<1){
					state++;
				}
				break;
			case 8:
			if(rotated<90){
					transform.Rotate(new Vector3(0,5,0));
					rotated+=5;
				}
				else{
					state++;
					rotated=0;
				}
				break;
			case 9:
			target=new Vector3(-37.3f, 0.49f, -14f);
				transform.position=Vector3.MoveTowards(rigidbody.position, target, 0.3f);
				if(Vector3.Distance(rigidbody.position, target)<1){
					state++;
				}
				break;

		}	
		}
		
	}

	

	bool forwCast(RaycastHit hit){
		return ((Physics.Raycast(rigidbody.position+new Vector3(0,2,1),transform.forward,out hit) && hit.collider.gameObject.name.Contains("Camera"))
				|| (Physics.Raycast(rigidbody.position+new Vector3(0,2,1),transform.forward+new Vector3(0,-0.05f,0),out hit) && hit.collider.gameObject.name.Contains("Camera"))
				|| (Physics.Raycast(rigidbody.position+new Vector3(0,2,1),transform.forward+new Vector3(0,-0.1f,0),out hit) && hit.collider.gameObject.name.Contains("Camera"))
				|| (Physics.Raycast(rigidbody.position+new Vector3(0,2,1),transform.forward+new Vector3(0,-0.2f,0),out hit) && hit.collider.gameObject.name.Contains("Camera")));
	}

	//0=no, 1=leftleft, 2=leftmid, 3=rightmid, 4=rightright
	int sideCast(RaycastHit hit){
		if(Physics.Raycast(rigidbody.position+new Vector3(0,2,1),transform.forward+new Vector3(-0.1f,0,0),out hit) && hit.collider.gameObject.name.Contains("Camera")){
			return 2;
		}
		else if(Physics.Raycast(rigidbody.position+new Vector3(0,2,1),transform.forward+new Vector3(-0.2f,0,0),out hit) && hit.collider.gameObject.name.Contains("Camera")){
			return 1;
		}
		else if(Physics.Raycast(rigidbody.position+new Vector3(0,2,1),transform.forward+new Vector3(0.1f,0,0),out hit) && hit.collider.gameObject.name.Contains("Camera")){
			return 3;
		}
		else if(Physics.Raycast(rigidbody.position+new Vector3(0,2,1),transform.forward+new Vector3(0.2f,0,0),out hit) && hit.collider.gameObject.name.Contains("Camera")){
			return 4;
		}
		return 0;
	}

	
}
