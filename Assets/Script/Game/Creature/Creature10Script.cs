﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creature10Script : MonoBehaviour {

	public static int state;
	Rigidbody rigidbody;
	Vector3 OriginalPosition;
	RaycastHit hit;
	int rotatedSearch=0;
	int rotated=0;
	bool searching;
	public static bool Door9Open;

	// Use this for initialization
	void Start () {
		state=0;
		rigidbody=GetComponent<Rigidbody>();
		OriginalPosition=rigidbody.position;
		searching=false;
		Door9Open=false;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 target = new Vector3(0,0,0);

		if(searching==true){
			if(forwCast(hit)){
				transform.Translate(new Vector3(0,0,0.5f), Space.Self);
			}
			else{
				switch(sideCast(hit)){
					case 1:
						transform.Rotate(new Vector3(0,8,0));
						rotatedSearch-=8;
						break;
					case 2:
						transform.Rotate(new Vector3(0,3,0));
						rotatedSearch-=3; 
						break;
					case 3:
						transform.Rotate(new Vector3(0,-3,0));
						rotatedSearch+=3;
						break;
					case 4: 
						transform.Rotate(new Vector3(0,-8,0));
						rotatedSearch+=8;
						break;
					default:
						if(rotatedSearch<360){
							transform.Rotate(new Vector3(0,-15,0));
							rotatedSearch+=15;
						}
						else{
							searching=false;
							rotatedSearch=0;
						}
						break;
				}
			}

		}
		else{

		if(Physics.Raycast(rigidbody.position+new Vector3(0,2,1),transform.forward,out hit) && hit.collider.gameObject.name.Contains("Camera")){
				searching=true;
		}
		else if(Physics.Raycast(rigidbody.position+new Vector3(0,2,1),transform.forward+new Vector3(0,-0.2f,0),out hit) && hit.collider.gameObject.name.Contains("Camera")){
				searching=true;
		}

			switch(state){
			case 1:
			target=new Vector3(3.88f,0.49f,33.5f);
				transform.position=Vector3.MoveTowards(rigidbody.position, target, 0.1f);
				if(Vector3.Distance(rigidbody.position, target)<1){
					state++;
				}
				break;
			case 2:
				if(Door9Open==false){
				target=new Vector3(3.88f,0.49f,33.5f);
					rigidbody.MovePosition(target);
					Door9Open=true;
				}
				if(DoorScript.opened==true){
				target=new Vector3(3.88f,0.49f,28);
					transform.position=Vector3.MoveTowards(rigidbody.position, target, 0.1f);
					if(Vector3.Distance(rigidbody.position, target)<1){
						state++;
					}
				}
				break;
			case 3:
			target=new Vector3(3.88f, 0.49f, 26.5f);
				transform.position=Vector3.MoveTowards(rigidbody.position, target, 0.1f);
				if(Vector3.Distance(rigidbody.position, target)<1){
					state++;
				}
				break;
			case 4:
				if(rotated>-80){
					transform.Rotate(new Vector3(0,-5,0));
					rotated-=5;
				}
				else{
					state++;
				}
				break;
			case 5:
				if(rotated<180){
					transform.Rotate(new Vector3(0,5,0));
					rotated+=5;
				}
				else{
					state++;
					rotated=0;
				}
				break;
			case 6:
			target=new Vector3(3.88f, 0.49f, 35);
				transform.position=Vector3.MoveTowards(rigidbody.position, target, 0.1f);
				if(Vector3.Distance(rigidbody.position, target)<1){
					state++;
				}
				break;
			case 7:
				if(rotated>-90){
					transform.Rotate(new Vector3(0,-5,0));
					rotated-=5;
				}
				else{
					state++;
					rotated=0;
				}
				break;
			case 8:
			target=new Vector3(-31,0.49f,35);
				transform.position=Vector3.MoveTowards(rigidbody.position, target, 0.1f);
				if(Vector3.Distance(rigidbody.position, target)<1){
					state++;
				}
				break;
			case 9:
				if(rotated>-180){
					transform.Rotate(new Vector3(0,-5,0));
					rotated-=5;
				}
				else{
					rotated=0;
					state++;
				}
				break;
			case 10:
			target=new Vector3(40,0.49f,35);
				transform.position=Vector3.MoveTowards(rigidbody.position,target, 0.1f);
				if(Vector3.Distance(rigidbody.position, target)<1){
					state++;
				}
				break;
			case 11:
				if(rotated<90){
					transform.Rotate(new Vector3(0,5,0));
					rotated+=5;
				}
				else{
					rotated=0;
					state++;
				}
				break;
			case 12:
			target=new Vector3(40,0.49f,-36.2f);
				transform.position=Vector3.MoveTowards(rigidbody.position,target, 0.1f);
				if(Vector3.Distance(rigidbody.position, target)<1){
					state++;
				}
				break;
			case 13:
				if(rotated<90){
					transform.Rotate(new Vector3(0,5,0));
					rotated+=5;
				}
				else{
					rotated=0;
					state++;
				}
				break;
			case 14:
			target=new Vector3(6.1f,0.49f,-36.2f);
				transform.position=Vector3.MoveTowards(rigidbody.position,target, 0.1f);
				if(Vector3.Distance(rigidbody.position, target)<1){
					state++;
				}
				break;
			case 15:
				if(rotated<180){
					transform.Rotate(new Vector3(0,5,0));
					rotated+=5;
				}
				else{
					rotated=0;
					state++;
				}
				break;
			case 16:
			target=new Vector3(40,0.49f,-36.2f);
				transform.position=Vector3.MoveTowards(rigidbody.position,target, 0.1f);
				if(Vector3.Distance(rigidbody.position, target)<1){
					state++;
				}
				break;
			case 17:
				if(rotated>-90){
					transform.Rotate(new Vector3(0,-5,0));
					rotated-=5;
				}
				else{
					rotated=0;
					state++;
				}
				break;
			case 18:
			target=new Vector3(40,0.49f,35);
				transform.position=Vector3.MoveTowards(rigidbody.position,target, 0.1f);
				if(Vector3.Distance(rigidbody.position, target)<1){
					state++;
				}
				break;
			case 19:
				if(rotated>-90){
					transform.Rotate(new Vector3(0,-5,0));
					rotated-=5;
				}
				else{
					rotated=0;
					state=8;
				}
				break;

				
			default:
				break;
			}
		}

		
	}

	bool forwCast(RaycastHit hit){
		return ((Physics.Raycast(rigidbody.position+new Vector3(0,2,1),transform.forward,out hit) && hit.collider.gameObject.name.Contains("Camera"))
				|| (Physics.Raycast(rigidbody.position+new Vector3(0,2,1),transform.forward+new Vector3(0,-0.05f,0),out hit) && hit.collider.gameObject.name.Contains("Camera"))
				|| (Physics.Raycast(rigidbody.position+new Vector3(0,2,1),transform.forward+new Vector3(0,-0.1f,0),out hit) && hit.collider.gameObject.name.Contains("Camera"))
				|| (Physics.Raycast(rigidbody.position+new Vector3(0,2,1),transform.forward+new Vector3(0,-0.2f,0),out hit) && hit.collider.gameObject.name.Contains("Camera")));
	}

	//0=no, 1=leftleft, 2=leftmid, 3=rightmid, 4=rightright
	int sideCast(RaycastHit hit){
		if(Physics.Raycast(rigidbody.position+new Vector3(0,2,1),transform.forward+new Vector3(-0.1f,0,0),out hit) && hit.collider.gameObject.name.Contains("Camera")){
			return 2;
		}
		else if(Physics.Raycast(rigidbody.position+new Vector3(0,2,1),transform.forward+new Vector3(-0.2f,0,0),out hit) && hit.collider.gameObject.name.Contains("Camera")){
			return 1;
		}
		else if(Physics.Raycast(rigidbody.position+new Vector3(0,2,1),transform.forward+new Vector3(0.1f,0,0),out hit) && hit.collider.gameObject.name.Contains("Camera")){
			return 3;
		}
		else if(Physics.Raycast(rigidbody.position+new Vector3(0,2,1),transform.forward+new Vector3(0.2f,0,0),out hit) && hit.collider.gameObject.name.Contains("Camera")){
			return 4;
		}
		return 0;
	}

}
