﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour {

	bool open;
	int rotate;
	int number=-1;
	public static bool opened;
	public static bool openCreature;

	// Use this for initialization
	void Start () {
		open=false;
		opened=false;
		openCreature=false;
		rotate=0;
		for(int i=0;i<MainGameScript.CollectableObj.Count;i++){
			if(MainGameScript.CollectableObj[i].GetName().Contains(this.name)){
				number=i;
				Debug.Log(this.name+" is "+number+" now");
				break;
			}
		}
		if(number==-1){
			Debug.Log("the number of "+this.name+" is -1, ERRORRR");
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(open==true || (Creature10Script.Door9Open==true && this.name.Contains("Room9"))){
			open=true;
			if(rotate<100){
				transform.Rotate(new Vector3(0,-5,0));
				rotate+=5; 
			}
			else{
				opened=true;
			}
		}

		if(open==false){
			opened=false;
			if(rotate>0){
				transform.Rotate(new Vector3(0,5,0));
				rotate-=5;
			}
		}

		if(this.name.Contains("Room5")&& MainGameScript.DinnerTime==true){
			open=true;
		}
	}

	void OnMouseDown(){
		Debug.Log("You click "+this.name);
		if(open==false && MainGameScript.CollectableObj[number].GetUsed()==true){
			open=true;
			Creature10Script.Door9Open=true;
			if(number==3 && Creature10Script.state==0){
				Creature10Script.state++;
			}
		}
		else{
			open=false;
			Creature10Script.Door9Open=false;
		}
	}
}
