﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundBGScript : MonoBehaviour {

	public AudioSource bgmusic;
	// Use this for initialization
	void Start () {
		bgmusic.Play();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
